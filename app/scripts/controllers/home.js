'use strict';

angular.module('ideaarqApp').controller('HomeCtrl', function ($scope) {
	$('.carousel').carousel({
  		interval: 2000
	})


	$('#constructionModal').modal('hide');
	$('.modal-backdrop.fade.in').remove();
    $("#wrapper").removeClass("toggled");
});
