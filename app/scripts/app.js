'use strict';
angular
  .module('ideaarqApp', ['ngResource', 'ngRoute'])

  .config(function ($routeProvider) {

    $routeProvider

      .when('/', {
        templateUrl: 'views/main.html',
        controller: 'MainCtrl'
      })

      .when('/test', {
        templateUrl: 'views/home.html',
        controller: 'HomeCtrl'
      })

      .otherwise({
        redirectTo: '/'
      });
  });
